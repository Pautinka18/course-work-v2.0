#include <iostream>
#include "race.h"
using namespace std;
class iter {
protected:
	race* i;
public:
	iter(race* i1) : i(i1) {}
	iter(const iter& x) : i(x.i) {}
	iter& operator=(const iter& x)
	{
		i = x.i; return *this;
	}
	race& operator*() const { return *i; }
	iter& operator++() { ++i; return *this; }
	iter operator--() {
		--i;
		return *this;
	}
	iter operator++(int) {
		i++;
		return *this;
	}
	iter operator--(int) {
		i--;
		return *this;
	
	}
	iter operator+(int n) { return iter(i + n); }
	iter operator-(int n) { return iter(i - n); }
	iter operator+=(int n) { i += n; return *this; }
	iter operator-=(int n) { i -= n; return *this; }
	bool operator==(const iter& x) const
	{
		return i == x.i;
	}
	bool operator!=(const iter& x) const
	{
		return i != x.i;
	}
	bool operator<(const iter& x) const
	{
		return i < x.i;
	}
	bool operator>(const iter& x) const
	{
		return i > x.i;
	}
	bool operator<= (const iter& x) const
	{
		return i <= x.i;
	}
	bool operator>=(const iter& x) const
	{
		return i >= x.i;
	}
	friend int operator-(const iter& x, const iter& y)
	{
		return x.i - y.i;
	}
};