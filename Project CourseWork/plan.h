#include "iterator.h"
#ifndef PLAN_H
#define PLAN_H

class plan {
protected:
	race* p;
	int size;
	static double a;
public:
	plan(int n) {
		p = new race[size = n];
		for (iter i = begin(); i != end(); ++i) {
			*i = race();
		}
	}

	~plan() {
		delete[] p;
	}
	friend istream& operator>>(istream& is, plan& s) {
		for (iter i = s.begin(); i != s.end(); ++i) {
			is >> *i;
			a += (*i).GetRaceKg();
			return is;
		}
	}

	friend ostream& operator<<(ostream& po, plan& s) {
		for (iter i = s.begin(); i != s.end(); ++i) {
			po << *i << endl;
			return po;
		}
	}
	double GetA() {
		return a;
	}
	iter begin() { return iter(p); }
	iter end() { return iter(p + size); }
};


#endif