#pragma once
#include <string>
#include  "Header.h"
#include "Auto.h"
#include <iostream>
using namespace std;
#ifndef _RACE_H
#define _RACE_H

class race : public automobile, public person {

public:
	void Set(double an, string au, string np, double t, double m);
	string GetData();
	string GetEnd();
	double GetKm();
	double GetRaceKg();
	double GetLosed();
	race();
	~race();
	virtual void printEnd();
	friend ostream& operator<<(ostream& os, race& a);
	friend istream& operator>>(istream& stream, race& a);

protected:

	string data;
	string endpoint;
	double km;
	double racekg;
	double losed;
};
#endif