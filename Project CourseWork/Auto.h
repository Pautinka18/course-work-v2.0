#pragma once
#include <string>
#include <iostream>
using namespace std;
#ifndef _AUTO_H
#define _AUTO_H

class automobile {

public:
	void Set(double an, string au, string np, double t, double m);
	string GetAutoname();
	double GetAutomass();
	double GetAutonumber();
	double GetAutooil();
	string GetAutoplace();
	automobile();
	automobile(double anb, string an, string ap, double am, double ao);
	~automobile();
	virtual void printAu();
protected:

	double autonumber;
	string autoname;
	string autoplace;
	double automass;
	double autooil;
};

#endif 